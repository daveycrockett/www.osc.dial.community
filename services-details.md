---
layout: page
title: "Available Services Menu"
---

Structural Assessment
=====================

The first step in determining the appropriate services to deliver and the specific goals to be achieved is to partner with the project to better understand the structure of the code and community.  All contracts for providing individual service modules, lifecycle packages or theme-based packages would begin with some form of structural assessment.  The [proposal template](proposal-template.html) and [maturity model](maturity-model.html) can aid the thought process during this phase, and provide examples of the sorts of aspects that are considered during this assessment.

Service Modules
===============

All mentorship & consulting services are available on a case-by-case basis as needed by projects. Projects will work with the OSC GAB via the onboarding process to determine the best solution for their specific needs, based upon capacity.

1. Governance assistance
   1. License & IP planning - Preparing a project to accept intellectual property contributions (code, documentation, etc.) from others via OSI/FSF licenses, copyright assignment, etc. Understanding the right license for a given project considering their plans & community.
   1. Financial/asset stewardship - Review of alternatives and advice for a project's current and future plans, to ensure an appropriate strategy for stewardship of IP and other tangible and non-tangible assets. Development of strategy & tactics for budgeting, fiscal planning, fundraising, financial distribution amongst community members.
    1. Product management & roadmap - Development of strategy, plans, policies, and processes to develop & maintain software product roadmaps. Ensure full stakeholder participation in all stages of product roadmap lifecycle. Plans, strategies, and tactics for maintaining product feature backlogs & release process.
 1. Community assistance
    1. Contributor onboarding - Design & development of strategies, documentation, and process to recruit individual & organizational project contributors of various types. Monitoring of newcomers to ensure motivations for participation, and adjust programs appropriately.
    1. Engagement & retention - Development of programs to increase participation of currently-available contributor ranks. Monitoring of exiting or inactive contributors to understand blockers to participation. Develop & deploy programs to ensure continual feedback from contributors.
    1. Diversity & inclusion - Outreach (directly and by participation in larger initiatives) to under-represented people in technology, open source software development, ICT4D, and other fields. Development of mentorship & education programs to maximize value & ensure optimal experiences.
    1. Recognition programs - Design, development, implementation, & monitoring of programs to recognize contributors, volunteers, and other community members. Solicitation of and action upon feedback from key community members to ensure their motivations for participation are being met.
    1. Metrics & evaluation - Design, development, implementation, & execution of metrics plan to measure activity of community and progress toward defined goals. Assistance with preparation of reports to stakeholders & funders regarding project activities.
 1. Engineering assistance
    1. Process Consulting - Helping to establish process around product development, code review and release management.  Code review process, for example, would establish rules around who would need to review changes and when, ideal throughput, approval/rejection criteria, etc.
    1. Interoperability - Researching opportunities to integrate with other projects, as well as standardizing a product's own API, potentially to adhere to well-established open standards.
    1. Software Development - Providing targeted code spikes to improve maturity, scalability or sustainability of a product.  An example could be a test coverage and refactoring effort, or improving deployment/packaging/configuration to be more user-friendly.
    1. Architectural Research & Systems Engineering - Providing recommendations on high-level component organization of a product, including how to reach higher scale. This could also include doing stress testing to better understand bottlenecks of the existing architecture.
 1. Infrastructure assistance
    1. Communications platforms - Gathering the requirements for the community (geographic distribution, connectivity/platform coverage, language support) and establishing recommendations for best tools and platform "etiquette."
    1. SQA tools & systems - Providing recommendations on continuous integration and delivery platforms, code coverage, etc.  This could also include helping to set up these systems for the project, and training other community members on how to maintain.
    1. Hosting & DevOps - Working to understand a project's physical infrastructure needs, for web hosting (in the case of SaaS), tool hosting, and demo & testing hosts.
    1. Documentation Systems - Incorporating auto-generated documentation into the software release cycle, determining best tools that fit the project requirements.

Lifecycle Service Packages
==========================

These service packages can be arranged depending on the lifecycle phase of a particular project. By default, all service modules will be scheduled to start and end within a single 3-month period. The services can be renewed or extended for more complicated or extended needs. All packages begin with a structural assessment, to understand the needs unique to each project.

``init``
--------

For projects that are newly launching, the following services may be most useful.

* Financial/asset stewardship
* License & IP planning
* Product management & roadmap
* Communications platforms

``clone``
---------

For projects that have established a process and have some users and contributors, but are looking to grow, these services should be considered.

* Contributor onboarding
* Diversity & inclusion
* Code review processes
* Documentation systems & planning

``rebase``
----------

Older projects may need to re-establish best practices or transition away from previous management structures.

* Product management & roadmap
* Engagement & retention
* Recognition programs

``commit``
----------

Established, mature products may be looking for a way to scale down their running costs and efforts, finalize documentation and figure out a way to enter "maintenance mode."

* Financial & asset stewardship
* SQA infrastructure
* Metrics & evaluation
* Testing & continuous delivery

Additional thematic-based programs
==================================

These additional programs are available based upon specific contextual needs of a project. As above, they are designed to be completed within a single 3-month period, but can be extended. All packages would also begin with a structural assessment, to understand the needs unique to each project.

Financial stability program
---------------------------

* License & IP planning
* Financial/asset stewardship
* Contributor onboarding
* Code review processes
* Communications platforms

Contributor optimization program
--------------------------------

* Engagement & retention
* Diversity & inclusion
* Recognition programs
* Code review processes
* Communications platforms

Product optimization program
----------------------------

* Product management & roadmap
* Metrics & evaluation
* Testing & continuous delivery
* Refactoring
* SQA tools & systems
* Documentation systems & planning
