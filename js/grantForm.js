
function toAscii(str) {
    var ch, i, result = '';
    for (i = 0; i < str.length; i++) {
        ch = str.charCodeAt(i);
        if (ch < 128) {
            result += str.charAt(i);
        } else {
            result += '\\u' + ch.toString(16);
        }
    }
    return result;
}

function validateField(selector) {
  fieldval = $(selector).val();
  if (typeof fieldval === 'string') {
    fieldval = fieldval.trim();
  }
  if (!fieldval) {
    $(selector).parent('fieldset').children('label').after("<span style=\"color:red\" class=\"formerror\">This field is required</span>");
    return false;
  }
  return true;
}

function postToGoogle() {

  text_fields = {
    'projectname' : '88610730',
    'projectwebsite' : '184743424',
    'projectdescription' : '1404364682',
    'contactname' : '1803408845',
    'contactemail' : '965443464',
    'dollaramount' : '1689862690',
    'grantdescription' : '868777940',
    'risk' : '1459992473',
    'impact' : '2077286407',
    'recipient': '1764527035'
  };

  checkbox_fields = {
     'impact' : '118067358',
     'independence' : '641719526',
     'consensusbuilding' : '679700331',
     'community' : '1596944984',
     'softwarequality' : '535535530',
     'softwarereleases' : '1807604943',
     'licenses' : '1337097391',
     'softwarecode' : '1355288531'
  };

  categoryLookup = {
    'Dirty' : 'Dirty Jobs',
    'Privacy' : 'Privacy and Responsible Data',
    'UX' : 'User Experience'
  }
  valid = true;
  $('.formerror').remove();
  for (field in text_fields) {
    valid = validateField('#' + field) && valid;
  }
  valid = valid && validateField("[name='category']:checked");

  if (!valid) {
    $('.button').before("<p style=\"color:red\" class=\"formerror\">Please fill in the required fields and try again.</p>")
    return;
  }

  var data = {}
  data['entry.2051131082'] = $("[name='category']:checked").val();
  for (field in text_fields) {
    data['entry.' + text_fields[field]] = $('#' + field).val();
  }
  for (field in checkbox_fields) {
    data['entry.' + checkbox_fields[field]] = $("[name='" + field + "']:checked").map(function() {
      return $(this).val();
    }).get();
  }

  $.ajax({
    url: "https://docs.google.com/forms/d/e/1FAIpQLSdq1PO6Z8tKecrn1KRNiMqzh-vBfxS-KIKQxgQeVxPj-QudwQ/formResponse",
    data: data,
    traditional: true,
    type: "POST",
    dataType: "xml",
    error: function() {
    },
    success: function() {
    }
  });

  var email = "Date: " + DateFormat.format.date(new Date(), "ddd").substring(0,3) + DateFormat.format.date(new Date(), ", d MMM yyyy HH:mm:ss +0000") + "\n";
  email += "To: dial+grants@discoursemail.com\n" +
            "From: " + toAscii($('#contactemail').val()) + "\n" +
            "Subject: " + categoryLookup[$("[name='category']:checked").val()] + " grant for " + toAscii($('#projectname').val()) + "\n" +
            "Content-Type: text/plain; charset=\"UTF-8\"\n\n\n\n";
  email += "**Project Name:** " + toAscii($('#projectname').val()) + "\n\n" +
          "**Project Website:** " + toAscii($('#projectwebsite').val()) + "\n\n" +
          "**Project Description:** " + toAscii($('#projectdescription').val()) + "\n\n" +
          "**Contact Name:** " + toAscii($('#contactname').val()) + "\n\n" +
          "**Contact Email Name:** " + toAscii($('#contactemail').val()) + "\n\n" +
          "**Dollar Amount:** " + toAscii($('#dollaramount').val()) + "\n\n" +
          "**Recipient:** " + toAscii($('#recipient').val()) + "\n\n" +
          "**Briefly Describe the work that would be done with this grant**\n" + toAscii($('#grantdescription').val()) + "\n\n" +
          "**What are the risks to completing the work, and how will you work to mitigate them?**\n" + toAscii($('#risk').val()) + "\n\n" +
          "**What is the foreseeable impact of this work?**\n" + toAscii($('#impact').val()) + "\n\n" +
          "**Maturity**\n" +
          "Impact: " + $("[name='impact']:checked").length + " / " + $("[name='impact']").length + "\n" +
          "Independence: " + $("[name='independence']:checked").length + " / " + $("[name='independence']").length + "\n" +
          "Consensus Building: " + $("[name='consensusbuilding']:checked").length + " / " + $("[name='consensusbuilding']").length + "\n" +
          "Community: " + $("[name='community']:checked").length + " / " + $("[name='community']").length + "\n" +
          "Software Quality: " + $("[name='softwarequality']:checked").length + " / " + $("[name='softwarequality']").length + "\n" +
          "Software Releases: " + $("[name='softwarereleases']:checked").length + " / " + $("[name='softwarereleases']").length + "\n" +
          "Licenses and Copyright: " + $("[name='licenses']:checked").length + " / " + $("[name='licenses']").length + "\n" +
          "Software Code: " + $("[name='softwarecode']:checked").length + " / " + $("[name='softwarecode']").length + "\n\n\n";

  $.ajax({
    url:"https://forum.osc.dial.community/admin/email/handle_mail",
    data: {
      email: email,
      api_key: 'e7fa7b1da788385cadcee9ed0dcf8a7a4cea99ad30da31ea1df6cb320b9eefef',
      api_username: 'dmccann'
    },
    type: "POST",
    error: function() {
    },
    success: function() {
    }
  });
  $('div.inner').html('<h2>Thank you</h2><p>Your application has been received. <strong>We will respond to confirm receipt of your proposal.</strong> We will reach out to finalists in June.</p><p><a href="grants.html"><span class="icon fa-long-arrow-left"></span>Back to grants</a></p>');
}
