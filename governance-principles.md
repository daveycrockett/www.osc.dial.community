---
layout: page
title: "Governance Principles"
---

Many free & open source software projects align with the ideals of “meritocracy”, but the traditional implementation of meritocracy as practiced in other communities often disadvantages certain voices. Rather, the community strives toward a world where technology leads to more equity in the world, and is creating an environment where all voices are heard and encouraged to collaboratively build the best solutions.

As a “holarchy”, the community has a unique process of leadership & decision making. Organized in projects teams, each group is charged to make its own decisions about its software product or initiative; however, decisions are not made “in a vacuum.” Each sub-group will need to consider:

* Pursuing additional co-creators,
* Finding ways for newcomers to participate,
* Coordinating with other projects and teams, and
* Building strategies on things like documentation, outreach, or diversity & inclusion.

Groups are encouraged to both self-organize, and leverage the collective resources of the OSC community to ensure their viability.

Importantly, we are a community of individuals. These individuals contribute to participating member projects (or to the OSC itself) by writing code, reviewing code, creating bugs, writing documentation, designing graphics, creating and running tests, etc. The community understands that some individuals may be doing this as part of their employment or other arrangement with a third-party organization. Organizations sponsoring such individuals are strongly encouraged to allow those individuals to offer their contributions on their own behalf and in their own name. Alternatively, an organization may submit (license) code (or other intellectual property assets) to the community and its sub-projects. However, the organization itself can not be a contributor. Organizations will be recognized as sponsors and/or partners of the OSC. The individuals facilitating that partnership will be recognized as contributors.
